# Rendezvous

This is the Android app for the [**Rendezvous**](https://framagit.org/hubzilla/addons/tree/master/rendezvous) location sharing addon for [**Hubzilla**](http://hubzilla.org), a powerful platform for creating interconnected websites featuring a decentralized identity, communications, and permissions framework.

This repository is packaged as an [**Android Studio**](https://developer.android.com/studio/) project.

_The app is currently a work in progress and is not yet in a useful state._
