package li.reticu.rendezvous;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

  private static final String LOG_TAG = MainActivity.class.getSimpleName();
  private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
  private boolean mPermissionsGranted;
  private List<String> mMissingPermissions;

  String INSTANCE_LATITUDE_MAIN_MAP = "latitudeMainMap";
  String INSTANCE_LONGITUDE_MAIN_MAP = "longitudeMainMap";
  String INSTANCE_ZOOM_LEVEL_MAIN_MAP = "zoomLevelMainMap";
  double DEFAULT_LATITUDE = 37.2;
  double DEFAULT_LONGITUDE = -79.9;
  double DEFAULT_ZOOM_LEVEL_MAIN_MAP = 6.0;

  MapView map = null;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    checkExternalStorageState();

    //handle permissions first, before map is created. not depicted here

    // check permissions on Android 6 and higher
    mPermissionsGranted = false;
    if (Build.VERSION.SDK_INT >= 23) {
      // check permissions
      Log.d("MainActivity", "Checking permissions...");
      mMissingPermissions = checkPermissions();
      mPermissionsGranted = mMissingPermissions.size() == 0;
    } else {
      mPermissionsGranted = true;
    }

    if (mPermissionsGranted) {
      //load/initialize the osmdroid configuration, this can be done
      Context ctx = getApplicationContext();
      Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
      //setting this before the layout is inflated is a good idea
      //it 'should' ensure that the map has a writable location for the map cache, even without permissions
      //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
      //see also StorageUtils
      //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's tile servers will get you banned based on this string

      //inflate and create the map
      setContentView(R.layout.activity_main);

      map = (MapView) findViewById(R.id.map);
      map.setTileSource(TileSourceFactory.MAPNIK);

      map.setBuiltInZoomControls(true);
      map.setMultiTouchControls(true);

      IMapController mController = map.getController();

      // initiate map state
      if (savedInstanceState != null) {
        // restore saved instance of map
        GeoPoint position = new GeoPoint(savedInstanceState.getDouble(INSTANCE_LATITUDE_MAIN_MAP, DEFAULT_LATITUDE), savedInstanceState.getDouble(INSTANCE_LONGITUDE_MAIN_MAP, DEFAULT_LONGITUDE));
        mController.setCenter(position);
        mController.setZoom(savedInstanceState.getDouble(INSTANCE_ZOOM_LEVEL_MAIN_MAP, DEFAULT_ZOOM_LEVEL_MAIN_MAP));
      } else {
        mController.setZoom(DEFAULT_ZOOM_LEVEL_MAIN_MAP);
        GeoPoint startPoint = new GeoPoint(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
        mController.setCenter(startPoint);
      }

    } else {
      showOnboarding();
    }
  }


  public void onResume() {
    super.onResume();
    if(map != null) {
      //this will refresh the osmdroid configuration on resuming.
      //if you make changes to the configuration, use
      //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
      //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
      map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    } else {
      showOnboarding();
    }
  }

  public void onPause() {
    super.onPause();
    if(map != null) {
      //this will refresh the osmdroid configuration on resuming.
      //if you make changes to the configuration, use
      //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
      //Configuration.getInstance().save(this, prefs);
      map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    } else {
      showOnboarding();
    }
  }

  /* Check which permissions have been granted */
  private List<String> checkPermissions() {
    List<String> permissions = new ArrayList<>();

    // check for location permission
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      // add missing permission
      permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
    }
    // check for storage permission
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
      // add missing permission
      permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    return permissions;
  }

  /* Checks the state of External Storage */
  private void checkExternalStorageState() {

    String state = Environment.getExternalStorageState();
    if (!state.equals(Environment.MEDIA_MOUNTED)) {
      Log.e(LOG_TAG, "Error: Unable to mount External Storage. Current state: " + state);

      // move MainActivity to back
      moveTaskToBack(true);

      // shutting down app
      android.os.Process.killProcess(android.os.Process.myPid());
      System.exit(1);
    }
  }

  public void showOnboarding() {
    if(mPermissionsGranted) {
      setContentView(R.layout.activity_main);
    } else {
      // point to the on main onboarding layout
      setContentView(R.layout.main_onboarding);

      // show the okay button and attach listener
      Button okayButton = (Button) findViewById(R.id.button_okay);
      okayButton.setOnClickListener(new View.OnClickListener() {
        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onClick(View view) {
          if (mMissingPermissions != null && !mMissingPermissions.isEmpty()) {
            // request permissions
            String[] params = mMissingPermissions.toArray(new String[mMissingPermissions.size()]);
            requestPermissions(params, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
          }
        }
      });
    }
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    if(map != null) {
      outState.putDouble(INSTANCE_LATITUDE_MAIN_MAP, map.getMapCenter().getLatitude());
      outState.putDouble(INSTANCE_LONGITUDE_MAIN_MAP, map.getMapCenter().getLongitude());
      outState.putDouble(INSTANCE_ZOOM_LEVEL_MAIN_MAP, map.getZoomLevelDouble());
    }
    super.onSaveInstanceState(outState);
  }


  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    switch (requestCode) {
      case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:	{
        Map<String, Integer> perms = new HashMap<>();
        perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        for (int i = 0; i < permissions.length; i++)
          perms.put(permissions[i], grantResults[i]);

        // check for ACCESS_FINE_LOCATION and WRITE_EXTERNAL_STORAGE
        Boolean location = perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        Boolean storage = perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

        if (location && storage) {
          // permissions granted - notify user
          Toast.makeText(this, R.string.toast_message_permissions_granted, Toast.LENGTH_SHORT).show();
          mPermissionsGranted = true;
          // switch to main map layout
          setContentView(R.layout.activity_main);
        } else {
          // permissions denied - notify user
          Toast.makeText(this, R.string.toast_message_unable_to_start_app, Toast.LENGTH_SHORT).show();
          mPermissionsGranted = false;
        }
      }
      break;
      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }
}
